#define _CRT_SECURE_NO_WARNINGS
#define K 256
#include<stdio.h>
#include<time.h>
int result(char str[][K], int num)
{
	int tmp[K] = { 0 };
	int r, l;
	int i = 2;
	srand(time(NULL));
	r = rand() % (num)+1;
	tmp[1] = r;
	while (i <= num)
	{
		r = (rand() % num) + 1;
		l = 1;
		for (int j = 1; j <= i + 1; j++)
			if (tmp[j] == r)
			{
				l = 0;
				break;
			}
		if (l == 1)
		{
			tmp[i] = r;
			i++;
		}
	}
	printf("\n");
	for (int i = 1; i <= num; i++)
		printf(" %s", str[tmp[i]]);
}
int main() {
	int num, i;
	char line[K][K] = { 0 };
	printf("Enter the number of lines, please\n");
	scanf("%d", &num);
	printf("\nEnter lines, please:\n", num);
	for (i = 0; i <= num; i++)
		fgets(line[i], 256, stdin);
	printf("\n String:\n");
	for (i = 1; i <= num; i++)
		printf("  %s", line[i]);
	printf("\n Result:");
	result(line, num);
	return 0;
}