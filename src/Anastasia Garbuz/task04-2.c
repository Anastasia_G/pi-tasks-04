#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
	srand(time(0));
	char str[30][256];
	int i, K = 0;
	printf("Enter number of strings, please: ");
	scanf("%d", &K);
	if (K > 20 || K < 1)
	{
		printf("ERROR\n");
		return 1;
	}
	puts("Enter strings:");
	for (i = 0; i <= K; i++)
	{
		fgets(str[i], 256, stdin);
		if (i != 0 && str[i][0] == '\n')
			break;
	}
	printf("\n");
	i = rand() % (K - 1 + 1) + 1;
	puts(str[i]);
	return 0;
}
